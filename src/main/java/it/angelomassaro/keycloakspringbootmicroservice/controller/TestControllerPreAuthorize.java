package it.angelomassaro.keycloakspringbootmicroservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testPreAuthorize")
public class TestControllerPreAuthorize {

	@RequestMapping(value = "/anonymous", method = RequestMethod.GET)
    public ResponseEntity<String> getAnonymous() {
        return ResponseEntity.ok("Hello Anonymous - @PreAuthorize");
    }

	@PreAuthorize("hasAnyRole('user','client-role-operatore')")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<String> getUser() {
        return ResponseEntity.ok("Hello User - @PreAuthorize");
    }

	@PreAuthorize("hasAnyRole('admin','client-role-amministratore')")
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ResponseEntity<String> getAdmin() {
        return ResponseEntity.ok("Hello Admin - @PreAuthorize");
    }

	@PreAuthorize("hasAnyRole('user','admin') or hasAnyRole('client-role-operatore','client-role-amministratore')")
    @RequestMapping(value = "/all-user", method = RequestMethod.GET)
    public ResponseEntity<String> getAllUser() {
        return ResponseEntity.ok("Hello All User - @PreAuthorize");
    }
    
}
