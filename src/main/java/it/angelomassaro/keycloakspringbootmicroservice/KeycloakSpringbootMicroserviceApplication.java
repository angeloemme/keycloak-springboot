package it.angelomassaro.keycloakspringbootmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "it.angelomassaro.keycloakspringbootmicroservice"} )
public class KeycloakSpringbootMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakSpringbootMicroserviceApplication.class, args);
	}

}
